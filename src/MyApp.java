import java.util.Scanner;

import javax.naming.spi.DirStateFactory.Result;

public class MyApp {
	static void printWelcome() {
		System.out.println("welcome to my app!!!");
	}

	static void printMenu() {
		System.out.println("--Menu--");
		System.out.println("1. Print Hello World N times");
		System.out.println("2. Add 2 number");
		System.out.println("3. Exit");
	}

	static int inputChoice() {
		int choice;
		Scanner sc = new Scanner(System.in);
		System.out.print("Please input your choice(1-3): ");
		while (true) {
			choice = sc.nextInt();
			if (choice >= 1 && choice <= 3) {
				return choice;
			}
			System.out.println("Error: Please input between 1-3");
		}
	}

	public static void main(String[] args) {
		int choice = 0;
		while (true) {
			printWelcome();
			printMenu();
			choice = inputChoice();
			switch (choice) {
				case 1:
					Scanner sc = new Scanner(System.in);
					System.out.println("Please input time: ");
					int time = sc.nextInt();
					for (int i = 0; i < time; i++) {
						System.out.println("Hello World!!!");
					}
					break;
				case 2:
					Scanner sc1 = new Scanner(System.in);
					int first, second;
					int result;
					System.out.print("Please input first number: ");
					first = sc1.nextInt();
					System.out.print("Please input second number: ");
					second = sc1.nextInt();
					result = first + second;
					System.out.println("Result = " + result);
					break;
				case 3:
					System.exit(0);
					System.out.println("Bye!!!");
					break;
			}

		}
	}
}
